<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Model\Products;

class ProductsController extends Controller
{
    //

    public function getAll(){

        try{
         
            $products=Products::All();
       
        }catch(\Exception $error){
       
            return response()->json([
            'success'   =>  false,
            'message'      => $error->getMessage(),
            ], 202); 
       
        }

        return response()->json([
            'success'   =>  true,
            'data'      =>  $products,
            ], 200);  

    }

    public function edit(Request $request){
        try{
            $postProduct=$request->input('data');    
      
            $id=$postProduct['id'];

            $product=Products::findOrFail($id);
            $product->description=$postProduct['description'];
            $product->price=$postProduct['price'];
            $product->save();
   
        }catch(\Exception $error){
            return response()->json([
                'success'   =>  false,
                'message'      => $error->getMessage(),
                ], 202); 
        }

        return response()->json([
            'success'   =>  true,
            'data'      =>  $product,
            ], 200);  

    }


    public function get(Request $request){

       try{
            $id=$request->input('id');

            $product=Products::findOrFail($id);
   
        }catch(\Exception $error){
            return response()->json([
                'success'   =>  false,
                'message'      => $error->getMessage(),
                ], 202); 
        }

        return response()->json([
            'success'   =>  true,
            'data'      =>  $product,
            ], 200);  


    }
    public function create(Request $request){

        try{
         $postProduct=$request->input('data');    
         $product=new Products();
         $product->description=$postProduct['description'];
         $product->price=$postProduct['price'];
         $product->save();

        }catch(\Exception $error){
            return response()->json([
                'success'   =>  false,
                'message'      => $error->getMessage(),
                ], 202); 
        }



        return response()->json([
            'success'   =>  true,
            'data'      =>  $product,
            ], 200);  


    }

    public function delete(Request $request){
        try{
            $postProduct=$request->input('data');    
      
            $id=$postProduct['id'];

            $product=Products::findOrFail($id);
            $product->delete();
   
        }catch(\Exception $error){
            return response()->json([
                'success'   =>  false,
                'message'      => $error->getMessage(),
                ], 202); 
        }

        $productMsg="Fue eliminado el maldito insecto";
        return response()->json([
            'success'   =>  true,
            'message'      =>  $productMsg,
            ], 200);  


    }
}
