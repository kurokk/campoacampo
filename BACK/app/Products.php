<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    public $timestamps = true;
    protected $table = 'products';
    protected $fillable = ['description', 'price'];
  
    public function dolar_price()
    {
        return $this->price*env('DOLAR');
    }
}
