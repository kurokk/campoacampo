<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      factory(App\Products::class, 2)->create()->each(function($u) {
        $u->product()->save(factory(App\Products::class)->make());
      });
    }
}
