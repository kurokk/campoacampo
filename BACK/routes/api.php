<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/getProducts', 'ProductsController@getAll');

Route::post('/editProduct', 'ProductsController@edit');

Route::post('/getProduct', 'ProductsController@get');

Route::post('/createProduct', 'ProductsController@create');


Route::post('/deleteProduct', 'ProductsController@delete');

Route::get('/getDolar', function(){
            return response()->json([
                'success'   =>  true,
                'valor'      => env('DOLAR'),
                ], 200);  
});