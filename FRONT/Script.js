const BackURL='http://127.0.0.1:8000';

function getValorDolar(){
    return 120;
};

const Dolar=getValorDolar();

function TablaProductos() {

    const [productos,actualizarProductos]= React.useState([]);
    const [modalCrear,verModalCrear]= React.useState('');
    const [modalEditar,verModalEditar]= React.useState('');
    
    ////////////MODAL////////////////
    function ModalEditar(props){

        var producto=props.producto;

        function editarProducto(id,desc,price){
            
            var errors=validatePrice(price);  

            if(errors){   
        
                let URL = BackURL+`/api/editProduct`;
                axios.interceptors.request.use(x=>{
                    return x;
                });

                axios.post(URL, {
                    data:{ "id":id,"description":desc, "price":price}
                    }, {
                        headers: {
                            'Content-Type': 'application/json',
                            'Access-Control-Allow-Origin': '*'
                        }
                    }).then((res) => {
                        if(res.data.message)
                        {
                            alert("El producto no fue creado por un error en sus valores!")
                        }else{
                            getAll();
                             document.getElementById("ModalEditar").style.display = "none"
                        }
                      
                    })
                    .catch((e) => {
                        //catch
                        console.error("ERROR ", e);
                    }).then(() => {
                        //props.history.push("/welcome");
                        //finalmente
                    })
            }else{
                console.log(errors);
            }        
        } 
        
        return (
            <div>
        
            <div className="modal" id="ModalEditar" tabIndex={-1} role="dialog" aria-labelledby="exampleModalLabel" style={{display: 'block', paddingRight: '15px'}}>
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">EDITAR</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={()=>{document.getElementById("ModalEditar").style.display = "none"}}>
                    <span aria-hidden="true"  data-dismiss="modal" >×</span>
                    </button>
                </div>
                <div className="modal-body">
                <div>
                    <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="inputGroup-sizing-default">ID</span>
                    </div>
                    <input type="text" className="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" id="id" defaultValue={producto.id} disabled/>
                    </div>
                    <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="inputGroup-sizing-default">Description</span>
                    </div>
                    <input type="text" className="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" id="desc" defaultValue={producto.description}/>
                    </div>
                    <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="inputGroup-sizing-default">Price</span>
                    </div>
                    <input type="text" className="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" id="price" defaultValue={producto.price}/>
                    </div>
             
                </div>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={()=>{document.getElementById("ModalEditar").style.display = "none"}}>Close</button>
                    <button type="button" className="btn btn-primary" onClick={()=>{editarProducto(document.getElementById("id").value,document.getElementById("desc").value,document.getElementById("price").value);}}>Save</button>
                </div>
                </div>
            </div>
            </div>
        </div>
        );
    }

    function ModalCrear(){

      

        function crearProducto(desc,price){
            
          var errors=validatePrice(price);  
            
         if(errors){   
            let URL = BackURL + `/api/createProduct`;
            axios.interceptors.request.use(x=>{
                 return x;
            });

            axios.post(URL, {
                data:{ "description":desc, "price":price}
                }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*'
                    }
                }).then((res) => {
                    if(res.data.message)
                    {
                        alert("El producto no fue creado por un error en sus valores!")
                    }else{
                        getAll();
                        document.getElementById("ModalCrear").style.display = "none"
                    }
                    
                })
                .catch((e) => {
                    //catch
                    console.error("ERROR ", e);
                }).then(() => {
                    //props.history.push("/welcome");
                    //finalmente
                })
          }else{
              console.log(errors);
          }      
        } 
        
        return (
            <div>
        
            <div className="modal" id="ModalCrear" tabIndex={-1} role="dialog" aria-labelledby="exampleModalLabel" style={{display: 'block', paddingRight: '15px'}}>
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={()=>{document.getElementById("ModalCrear").style.display = "none"}}>
                    <span aria-hidden="true"  data-dismiss="modal" >×</span>
                           </button>
                </div>
                <div className="modal-body">
                <div>
                    <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="inputGroup-sizing-default">Description</span>
                    </div>
                    <input type="text" className="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" id="desc" required/>
                    </div>
                    <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="inputGroup-sizing-default">Price</span>
                    </div>
                    <input type="text" className="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" id="price" required/>
                    </div>
                </div>
                
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={()=>{document.getElementById("ModalCrear").style.display = "none"}}>Close</button>
                    <button type="button" className="btn btn-primary" onClick={()=>{crearProducto(document.getElementById("desc").value,document.getElementById("price").value);}}>Save</button>
                </div>
                </div>
            </div>
            </div>
        </div>
        );
    }

    function validatePrice(price){
        
        var error= !isNaN(parseFloat(price)) && isFinite(price);
        if(error==false)
        {
          alert("El valor no es un Decimal!");  
          return false;
        }  
        return true;  
    }
    /////////////////////////////////
    function getAll() {
        let URL = BackURL + `/api/getProducts`;
        axios.interceptors.request.use(x=>{
            console.log("cargando...");
            return x;
        });

        axios.get(URL, {
            }, {
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then((res) => {
               console.log('realizado...');
               actualizarProductos(res.data.data);
            })
            .catch((e) => {
                //catch
                console.error("ERROR ", e);
            }).then(() => {
                //props.history.push("/welcome");
                //finalmente
            })
    }
    
    function crear() {
         verModalCrear(<ModalCrear/>);    
    }

    function editar(producto) {
         verModalEditar(<ModalEditar producto={producto}/>);    
    }

    function borrar(id) {
        var r = confirm("Seguro desea eliminar el Producto "+id+"?");
        if (r == true) {
            let URL = BackURL+`/api/deleteProduct`;

        axios.interceptors.request.use(x=>{
            console.log("cargando...");
            return x;
        }); 

        axios.post(URL, {
                data: { "id":id}
            }, {
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then((res) => {
                console.log(res);
                alert("Se borro correctamente!");
                getAll();
            })
            .catch((e) => {
                //catch
                console.error("ERROR ", e);
            }).then(() => {
                //props.history.push("/welcome");
                //finalmente
            })
        }
    }
   
    React.useEffect(()=>{
        getAll();

    },[]); 
    
    
       

    return (
              <div className="container" style={{marginTop: '30px'}}>
                <div style={{display: 'flexbox'}}>
                    <span><h1 style={{width: '80%'}}>PRODUCTS</h1></span> 
                    <span style={{float: 'right',marginBottom:'10px'}}>     
                        <a  href="#" onClick={()=>{crear()}} className="btn btn-primary btn-outline " data-toggle="modal" data-target="#ModalCrear" style={{margin: '2px'}} >
                            NEW +
                        </a>
                </span></div> 
                <table className="table table-fixed table-dark mt3">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>DESCRIPTION</th>
                    <th>PRICE</th>
                    <th>DOLAR</th>
                    <th>ACTIONS</th>
                    </tr>
                </thead>
                <tbody>
                    {productos.length>0 && productos.map((producto)=>{
                        return (
                        <tr key={producto.id}>
                        <td scope="row">{producto.id}</td>
                        <td scope="col">{producto.description}</td>
                        <td scope="col">AR${producto.price}</td>
                        <td scope="col">US${Math.round((producto.price/Dolar) * 100)/100}</td>
                        <td scope="col">
                         <a href="#" onClick={()=>{editar(producto)}} className="btn btn-primary btn-outline "  data-toggle="modal" data-target="#exampleModal" style={{margin: '2px'}}>
                            <i className="fa fa-pencil fa-1x" />
                        </a>
                        <a href="#" onClick={()=>{borrar(producto.id)}} className="btn btn-danger  btn-outline"   style={{margin: '2px'}}>
                            <i className="fa fa-times fa-1x" />
                        </a>
                        </td>
                        </tr>
                            );
                        })}  

                    </tbody>
                    </table>
                    {modalEditar}
                    {modalCrear}  
                </div>
            );

}

const TablaProd = <TablaProductos />;

ReactDOM.render(
TablaProd,
document.getElementById('app')
);