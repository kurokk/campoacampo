Para levantar este proyecto se deben realizar los siguientes pasos:
-tener php 7.3 o superior.
-tener composer instalado.
-crear una base de datos mysql.
-posicionarse sobre el directorio raiz del proyecto y configurar el archivo .env [sino existe crearlo].

_______________________________EJEMPLO DE CONTENIDO DE .env_______________________________________

APP_NAME=Laravel                                                                 
APP_ENV=local
APP_KEY=base64:D0mKz4hlxSiDViDH5Hpg1pce8/W+/aQiKmnsSvHwXps=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=tu_database
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

#---------DOLAR-----------

DOLAR=120
_____________________________________________________________________________________________


-sobre la raiz ejecutar los siguientes comandos:
 
 composer install [instala las dependencias necesarias para el funcionamiento del proyecto]
 php artisan migrate [genera las tablas del proyecto]
 php artisan serve [levanta un servidor incorporado de PHP que para acceder se debe ingresar a http://127.0.0.1:8000].

De esta manera su servidor y backend esta funcionando.


